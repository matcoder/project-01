Northlakes Class Catch-up web app
=================================

Frequently used files:
----------------------

HTML: 
The home page:  app/views/page/default.html.erb
The about page:  app/views/page/about.html.erb
The courses page:  app/views/courses/index.html.erb
The resource page:  app/views/resources/show.html.erb

CSS:
Application-wide:  app/assets/stylesheets/application.css.scss
Course pages:  app/assets/stylesheets/courses.scss
Resource page:  app/assets/stylesheets/resources.scss


To deploy to Heroku
-------------------
1. commit and changes
git add .
git commit -m "update message goes here"

2. push to heroku
git push heroku master

3. migrate / seed if necessary (first run for sure!)
heroku rake db:migrate
heroku rake db:seed

To check server logs for errors
-------------------------------
heroku logs 

Heroku Config variables
-----------------------
1. For super user:
* ADMIN_EMAIL
* ADMIN_PASS
2. For AWS S3 storage of Paperclip attachments:
* AWS_ACCESS_KEY_ID
* AWS_REGION
* AWS_SECRET_ACCESS_KEY
* S3_BUCKET_NAME
3. For Sendgrid email service:
* SENDGRID_PASSWORD
* SENDGRID_USERNAME
