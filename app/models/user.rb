class UserValidator < ActiveModel::Validator
  def validate(record)
    unless record.email.end_with? "@education.nsw.gov.au"
      record.errors[:base] << "Only school email accounts may be used"
    end
  end
end
 
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  validates_with UserValidator
end
