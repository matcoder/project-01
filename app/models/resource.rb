class Resource < ActiveRecord::Base
  belongs_to :course
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def is_owned_by(user)
    return false if !user
    user.id == uploaded_by_user_id
  end

end
