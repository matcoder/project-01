json.extract! resource, :id, :course_id, :lesson_date, :url, :created_at, :updated_at
json.url resource_url(resource, format: :json)