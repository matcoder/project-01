class AddIsSysadminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_sysadmin, :boolean
  end
end
