class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.belongs_to :course, index: true, foreign_key: true
      t.datetime :lesson_date
      t.string :url

      t.timestamps null: false
    end
  end
end
