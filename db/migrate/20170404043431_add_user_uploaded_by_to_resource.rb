class AddUserUploadedByToResource < ActiveRecord::Migration
  def change
    add_column :resources, :uploaded_by_user_id, :integer
  end
end
