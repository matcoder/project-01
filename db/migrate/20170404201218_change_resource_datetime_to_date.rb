class ChangeResourceDatetimeToDate < ActiveRecord::Migration
  def change
    change_column :resources, :lesson_date, :date
  end
end
